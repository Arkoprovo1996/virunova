function loginGuest()
{
    document.getElementsByClassName('login_page')[0].style.display = 'none';
    document.getElementsByClassName('about')[0].style.display = 'block';
    document.getElementsByClassName('classroom_tab')[0].style.display = 'block';
    document.getElementsByClassName('bibcache_tab')[0].style.display = 'block';
}

function logOut()
{
    document.getElementsByClassName('login_page')[0].style.display = 'block';
    document.getElementsByClassName('about')[0].style.display = 'none';
    document.getElementsByClassName('classroom_tab')[0].style.display = 'none';
    document.getElementsByClassName('bibcache_tab')[0].style.display = 'none';
}

function openBC()
{

}

function openClassRoom()
{
    
    ID = '';
    i = 5;
    while(i-- != 0)
        ID += String.fromCharCode(Math.random() * 26 + 65);
    jDiv = document.getElementsByClassName('jitsi')[0];
    jDiv.innerHtml = '<iframe src="https://meet.jit.si/' + ID + '" /></iframe>';
}